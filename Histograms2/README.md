# Array of histograms

Create a program, where you read the file `../Trees/events.txt` and create 10
histograms of _&phi;_<sub>h</sub> separately for positive and negative hadrons
and in 5 bins of _z_,
let's say _z_ &isin; [0.2, 0.25), _z_ &isin; [0.25, 0.35), _z_ &isin; [0.35, 0.5), _z_ &isin; [0.5, 0.65) and _z_ &isin; [0.65, 0.85).

Some tips:

You can use a 2-dimensional array of histograms:
```cpp
const int nq = 2;
const int nz = 5;
TH1D* hphi[nq][nz];	// an array of nq x nz histogram pointers
```

The histograms can be initialized one by one:
```cpp
hphi[0][0] = new TH1D("hphi_q0_z0","h^{#minus}, z0;#phi_{h}",32,-M_PI,M_PI);
...
```

or in a loop (but you have to give unique names to them!):

```cpp
for (int iq = 0; iq < nq; iq++)
{
	for (int iz = 0; iz < nz; iz++)
	{
		char buff[64];	// an array of 64 characters
		sprintf(buff,"hphi_q%i_z%i",iq,iz);	// print to the array
		hphi[iq][iz] = new TH1D(buff,";#phi_{h}",32,-M_PI,M_PI);
	}
}
```

Then, for each line in events.txt you have to decide which bin it belongs to...
For the charge it is easy -- one simple 'if' does the job. Or you can calculate
the charge index (0 for negative and 1 for positive) from q.
For the z-bins, you can of course do 5 ifs. Or you can declare an array of bin
limits (or borders) before the loop and test in a loop where the event belongs.

```cpp
float zBins[nz+1] = {0.2, 0.25, 0.35, 0.5, 0.65, 0.85};
...
// in the main loop, find the index of the bin where 'z' belongs:
for (int i = 0; i < nz; i++)
{
	if (z >= zBins[i] && z < zBins[i+1])
		{...}
}
```
