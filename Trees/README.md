# ROOT tree reading, 3-vectors and Lorentz vectors

The file events.root contains a ROOT tree
([TTree class](https://root.cern.ch/doc/master/classTTree.html), [ROOT manual](https://root.cern/manual/trees/))
with an already pre-selected sample of hadrons produced in deep inelastic scattering (DIS) of negative muons off a liquid hydrogen target.
In fact, they are the very same events as in
[../Histograms/events.txt](../Histograms/events.txt),
they are just many more of them (about 500&nbsp;000).
This time they are 'raw' &ndash; the file contains only observables
directly measured by the experiment, that is the momenta of the incoming
muon (denoted _l_), the scattered muon (_l_') and of the hadron (_P_<sub>h</sub>),
all of them in the laboratory system
(axis _Z_ pointing roughly along the beam and the axis _Y_ pointing upwards).

1. Write a program or a macro that opens the file, loads the tree and loops through all the hadrons (see `readTreeMacro.cpp` for an example).

2. Construct `TVector3` objects representing _l_, _l_' and _P_<sub>h</sub> and set the tree branch addresses to these variables.

3. Calculate the invariants _x_, _Q_<sup>2</sup>, _W_ and _s_
(_s_ is the total 4-momentum squared).
You can find the variables explained for example in Chapter 5 of this [free texbook](https://www.fzu.cz/~chyla/lectures/text.pdf).
Try using `TLorentzVectors` _l_ and _l_' to calculate the virtual photon 4-momentum
_q_ and then the _Q_<sup>2</sup>.

4. Calculate _z_ being the energy of the hadron (assuming pion mass) in lab divided by the energy lost by the muon _z_ = (_E_ - _E_') / _E_<sub>h</sub>.

5. Compare the first couple of events with the file [../Histograms/events.txt](../Histograms/events.txt). The values of the variables should be the same.

6. Translate the vector _P_<sub>h</sub> into the gamma-nucleon system
(GNS, with _Z_ along the virtual photon momentum _q_ = _l_' &minus; _l_ and _X_ lying in the muon plane, see e.g. [Sec. 1.3 of this thesis](https://wwwcompass.cern.ch/compass/publications/theses/2021_phd_moretti.pdf)).
For this, I suggest to construct the basis vectors of the GNS and then to
obtain the three _P_<sub>h</sub><sup>GNS</sup> components from scalar products of _P_<sub>h</sub> with the three basis vectors.

7. Save the _P_<sub>h</sub><sup>GNS</sup> and the variables _x_, _Q_<sup>2</sup>, _W_, _z_
in a new ROOT tree.


## ROOT tree creation and writing
The program `tree.cpp` shows how such a tree can be produced.
It reads a text file with the components of the three vectors and saves them
to a ROOT tree. However, the input file `rawEvents.txt` contains only 100 events,
much less than the file `events.root`, not to overload the GIT repository with
large files.
