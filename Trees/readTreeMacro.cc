/*
*	Simple ROOT macro to read a ROOT tree
*/

void readTreeMacro(const char* fileName)
{
	TFile* file = new TFile(fileName,"read");	// open file for reading
	if (!file->IsOpen())
		return;

//	file->ls();		// to print what is inside

	TTree* tree = NULL;
	// load a TTree object 'SIDIS' from the file
	// and set the value of the variable 'tree' to its address:
	file->GetObject("SIDIS", tree);

	if (tree == NULL)	// if the loading failed (e.g. wrong name)
		return;
	printf("The tree 'SIDIS' was loaded from file 'fileName'.\n");
	printf("The tree contains %lli events.\n",tree->GetEntries());

	tree->Print();		// to print the structure ('branches') of the tree

	// prepare objects representing things stored in the tree:
	TVector3* l3 = new TVector3();	// 3-momentum of the beam muon

	// tell the tree the addresses of the objects to be read from it:
	tree->SetBranchAddress("l", &l3);

	// loop over the events in the tree (like lines in a table)
//	long int N = tree->GetEntries();	// all events
	long int N = 10;                	// only first 10 events (for tests)
	for (int i = 0; i < N; i++)
	{
		// set all the associated objects to the values of the i-th event
		tree->GetEvent(i);

		// just to check that it works:
		printf("l = (%6.2f, %6.2f, %6.2f) GeV/c    %f\n", l3->X(), l3->Y(), l3->Z(), l3->Mag());
	}

	file->Close();
}
