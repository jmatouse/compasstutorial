/****************************************************************************
*
*	use root libraries in a simple program
*
*****************************************************************************
*
* Jan Matousek, 2019-10-07
*/

#include <stdio.h>		// for printf
#include <math.h>		// for math functions and constants like pi
#include <fstream>		// for file reading with a stream
#include <string>		// for std::string
#include <sstream>		// for stream-based conversion from string to numbers

// include ROOT libraries that you need:
#include "TCanvas.h"
#include "TVector3.h"
#include "TFile.h"
#include "TTree.h"

using namespace std;

int main(int argc, char **argv)
{
	// ******** ROOT tree making exercise *********************************
	// With this example program, try to open the file rawEvents.txt 
	// (or first do head -100 rawEvents.txt > rawEventsHead.txt for testing)
	// They contain columns with numbers describing 3-momenta of the incoming
	// muon (denoted l), the scattered muon (l') and of the hadron (P_h) in the
	// laboratory system (axis Z pointing roughly along the beam and axis Y upwards)
	// **********************************************************************
	printf("**** tree *****\n");
	if (argc < 2)
	{
		printf("Usage: tree INFILENAME [TREENAME]\n");
		return 1;
	}
	const char* fname = argv[1];

	ifstream file(fname);	// take the 1st argument provided as the filename
	if (!file.is_open())
	{
		printf("Failed to open %s\n",fname);
		return 1;
	}
	printf("Opened file %s.\n",fname);

	TFile* outFile = NULL;
	if (argc == 3)
	{
		outFile = new TFile(argv[2],"create");
		if (!outFile->IsOpen())
		{
			file.close();
			return 2;
		}
	}

	TTree* tr = new TTree("SIDIS","DIS and hadrons");
	TVector3* vl   = new TVector3();
	TVector3* vlPr = new TVector3();
	TVector3* vPh  = new TVector3();
	int qh;
	tr->Branch("l",   &vl);
	tr->Branch("lPr", &vlPr);
	tr->Branch("Ph",  &vPh);
	tr->Branch("qh",  &qh);

	//
	// read the numbers from the file and fill the tree:
	//
	double l[3], lPr[3], Ph[3];
	string line;
	while (getline(file, line))
	{
		if (line[0] == '#')	// if the first character is '#' -> just a comment
		{
			printf("Comment: '%s'\n",line.c_str());
			continue;
		}
		istringstream stream(line);
		if (stream >> l[0] >> l[1] >> l[2] >> lPr[0] >> lPr[1] >> lPr[2]
				>> Ph[0] >> Ph[1] >> Ph[2] >> qh)
		{
			vl->SetXYZ(l[0],l[1],l[2]);
			tr->Fill();
		}
		else
			printf("Failed to understand line '%s'\n",line.c_str());
	}
	file.close();
	printf("\n****** finished reading file ******\n");
	printf("saved %lli etries to tree.\n",tr->GetEntries());
	if (outFile != NULL)
	{
		tr->Write();
		outFile->Close();
	}
	return 0;
}
