/****************************************************************************
*
*	c++ input and output
*
*****************************************************************************
*
* Jan Matousek, 2019-10-07
*/

#include <stdio.h>		// for printf etc.
#include <iostream>		// for the streams (cout <<, cin...)
#include <math.h>		// for math functions and constants like pi
#include <fstream>

using namespace std;

int main(int argc, char **argv)
{
	printf("**** cpp_in-out 2 *****\n");
	if (argc < 2)
	{
		printf("Usage: cpp_in-out INFILENAME\n");
		return 1;
	}
	const char* fname = argv[1];

	ifstream file(fname);	// take the 1st argument provided as the filename
	if (!file.is_open())
	{
		cout << "Failed to open " << fname << endl;
		return 1;
	}

	//
	// count the number of lines in the file:
	//
	double tmp;
	int n = 0;
	while (file >> tmp >> tmp >> tmp)	// read 3 numbers as long as you can
		n++;
	printf("Opened file %s with %i lines.\n",fname,n);

	// the files data0.txt, data0.txt and data0.txt contain 3 columns of numbers
	double* x = new double[n];
	double* y = new double[n];
	double* e = new double[n];	// uncertainty (1 sigma)

	// before reading the file again we need to tell the stream to
	// return to the beginning:
	file.clear(); // clear the error state that occured when it reached the end
	file.seekg(0,ios::beg);	// go to the beginning of the file
	//
	// read the numbers from the file:
	//
	int i = 0;
	while (i < n && file >> x[i] >> y[i] >> e[i])
	{
		printf("read line: %10f %15f %15f\n",x[i],y[i],e[i]);
		i++;
	}

	//
	//	 Do something with the numbers...
	//		- try to calculate the average and the standard deviation
	//		- if you skip the lines that have 0 error (3rd column = 0),
	//		  you should get (for an error-weighted mean):
	//			- data0.txt: <y> = 5792.45
	//			- data4.txt: <y> = 1341.84
	//			- data7.txt: <y> = 1231.28

	return 0;
}
