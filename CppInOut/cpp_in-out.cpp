/****************************************************************************
*
*	c++ input and output
*
*****************************************************************************
*
* Jan Matousek, 2019-10-07
*/

#include <stdio.h>		// for printf etc.
#include <iostream>		// for the streams (cout <<, cin...)
#include <math.h>		// for math functions and constants like pi
#include <fstream>

using namespace std;

int main(int argc, char **argv)
{
	// the old c-style printing to the standard output
	printf("**** cpp_in-out *****\n"); // \n ends line in linux
	printf("A nice feature of printf is the easy formatting:\n");
	printf("\tpi = %f\n",M_PI);			// \t is a tabulator
	printf("\tpi = %.3f  (3 digits)\n",M_PI);
	printf("\tpi = %.10f (%i digits)\n\n",M_PI,10);

	// c++ streams are an alternative
	cout << "One can also use streams for the printouts." << endl;
	// endl puts the correct line ending according to the environment
	cout << "\tpi = " << M_PI << endl;
	cout <<"It can be also formatted, ";
	cout << "but I don't remember how (but Google knows...)" << endl << endl;

	// Reading from the standard input is very easy with streams:
	cout << "Give me the name of a file: ";
	string fname;
	getline(cin,fname);
	cout << "The number of lines to be read in " << fname << ":";
	int nlines = 0;
	cin >> nlines;

	ifstream file(fname.c_str());	// c_str converts string to char[]
	if (!file.is_open())
	{
		cout << "Failed to open " << fname << endl;
		return 1;
	}

	// the files data0.txt, data0.txt and data0.txt contain 3 columns of numbers
	double* x = new double[nlines];
	double* y = new double[nlines];
	double* e = new double[nlines];

	int i = 0;
	while (i < nlines && file >> x[i] >> y[i] >> e[i])
	{
		printf("read line %02i  %6.3f  %6.1f  %6.2f\n",i+1,x[i],y[i],e[i]);
		i++;
	}
	file.close();
	printf("\n\n");
	for (int j = 0; j < nlines; j++)
	{
		// the data are saved in the arrays, so they can be used later:
		printf("%6.3f  %6.1f  %6.2f\n",x[j],y[j],e[j]);
	}

	return 0;
}
