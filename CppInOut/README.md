# Input and output in C++

## The source codes

The file `cpp_in-out.cpp` contains a very simple C++ program. A C++ program must contain the function named `main`, which will be run when the program is launched. In it you will find several examples how text can be prited on the terminal (`printf()` or `cout`) and how an input from the user can be read (`getline` or `cin`). Finally, reading from a text file containing columns of numbers is shown.

The second program `cpp_in-out2.cpp` demonstrates how the program can read arguments with which the program is called. The advantage of passing information to the program via the arguments is an easy automation - one program can be used with different input files and it can be called by scripts.

The source files are quite self-documenting and contain a lot of comments.

## Compilation

On Linux, you can compile the program source code into an executable with
```bash
g++ cpp_in-out.cpp -o cpp_in-out.exe
```

The compilation can be simplified with `make`.
It is a program that reads a simple script usually called `Makefile`.
In the script, one specifies the _targets_ (files to be produced),
the _dependences_ (files that are needed to produce them),
and a _recipe_ to produce them.
When you run `make [TARGETS]`, it reads the `Makefile`.
If any of the targets given does not exist or if any of their dependences
is newer than the target (e.g we have edited the source code),
it runs the recipe. When no target is specified, a default target `all` is used.

Before running the recipe it actually checks if there are recipes for the
dependences and first runs them, so one can create a chain of files where
one depends on another.

General strructure of a Makefile rule is:
```make
target: dependence1 dependence2...
	recipe
```
You can find an example Makefile that compiles the two programs described above.
