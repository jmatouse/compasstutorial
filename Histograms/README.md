# ROOT Histograms

There is a text file where every line represents one hadron produced in deep inelastic scattering (DIS) of muons.
The columns contain kinematic variables commonly used to describe the process:
```
x    Q^2 (GeV)^2     q_h     z      P_hT (GeV)     phi_h (rad)
```
where _x_, _Q_<sup>2</sup> and _z_ are standard variables that can be found e.g. in [this textbook](https://www.fzu.cz/~chyla/lectures/text.pdf),
_q_<sub>h</sub> is the hadron charge, _P_<sub>hT</sub> is its transverse momentum with respect to the momentum transfer in the process and _&phi;_<sub>h</sub> is
the angle between the transverse momentum and the muon scattering plane.
However, the meaning of the variables is not central to this exercise.


1. Check the program `histo.cpp`, which reads the input text file
(`events.txt`) and creates 1-dimensional histograms
of two variables from the file.

2. Check `histo_logbins.cpp`, where a trick to obtain logarithmic binning
is presented (the default is equidistant binning).

3. Make histograms of all the variables in the tree (_x_, _Q_<sup>2</sup>...)
Make a 2-dimensional histogram of _Q_<sup>2</sup> with respect to _x_.

4. For the hadron variables (_P_<sub>hT</sub> and _&phi;_<sub>h</sub>), do it separately for positive and negative hadrons.

4. Create a `TFile` and save the histograms into it. Open the `TFile` in an
interactive ROOT session and draw the histograms with different options.
Plot a projection and several slices of the _xQ_<sup>2</sup> 2D histogram.
