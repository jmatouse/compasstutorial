/****************************************************************************
*
*	use root libraries in a simple program
*
*****************************************************************************
*
* Jan Matousek, 2019-10-07
*/

#include <stdio.h>		// for printf
#include <math.h>		// for math functions and constants like pi
#include <fstream>		// for file reading with a stream
#include <string>		// for std::string
#include <sstream>		// for stream-based conversion from string to numbers

// include ROOT libraries that you need:
#include "TCanvas.h"
#include "TH1.h"		// 1-dimensional histograms

using namespace std;

// A function to set logarithmic bins to an axis
// First construct the histogram putting the desired decimal exponents
// as the axis minimum and maximum. Then call this function...
// (e.g. TH1D h(name,title,N,A,B); SetLogBins(h.GetXaxis());
//  will result in a histogram with N bins between 10^A to 10^B)
void SetLogBins(TAxis* axis)
{
	int nbins = axis->GetNbins();
	Axis_t from = axis->GetXmin();
	Axis_t to = axis->GetXmax();
	Axis_t width = (to - from) / nbins;
	Axis_t* newBins = new Axis_t[nbins + 1];
	
	for (int i = 0; i <= nbins; i++)
		newBins[i] = pow(10, from + i * width);

	axis->Set(nbins, newBins);
	delete[] newBins;
}


int main(int argc, char **argv)
{
	// *** ROOT histogramming exercise: variant with logarithmic bins in x ***
 	// With this example program, try to open the file events.txt 
	// or events_tail100.txt (containing the last 100 lines of events.txt)
	// They contain columns with numbers describing 
	// semi-inclusive deep inelastic events (SIDIS):
	// x    Q^2 (GeV)^2     q_h     z      P_hT (GeV)     phi_h (rad)
	// ***********************************************************************
	printf("**** histo *****\n");
	if (argc < 2)
	{
		printf("Usage: histo INFILENAME\n");
		return 1;
	}
	const char* fname = argv[1];

	ifstream file(fname);	// take the 1st argument provided as the filename
	if (!file.is_open())
	{
		printf("Failed to open %s\n",fname);
		return 1;
	}
	printf("Opened file %s.\n",fname);
	//
	//	Create a histogram:
	//
	TH1D* hx = new TH1D("x","Hadrons in DIS;#it{x}",150,-3,0);
	SetLogBins(hx->GetXaxis());
	//
	// read the numbers from the file and fill the histogram:
	//
	double x, Qsq, z, pT, phi;
	int q;
	string line;
	while (getline(file, line))
	{
		if (line[0] == '#')	// if the first character is '#' -> just a comment
		{
			printf("Read comment: '%s'\n",line.c_str());
			continue;
		}
		istringstream stream(line);
		if (stream >> x >> Qsq >> q >> z >> pT >> phi)
		{
			hx->Fill(x);
		}
		else
			printf("Failed to understand line '%s'\n",line.c_str());
	}
	TCanvas c("canv","",800,600); // create a canvas named canv with 800x400 px
	c.SetLogx(); // set logarithmic scale for the drawing ( != logarithmic bins)
	hx->Draw();
	c.Print("histo_logbins.pdf");
	return 0;
}
