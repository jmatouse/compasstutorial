/****************************************************************************
*
*	use root libraries in a simple program
*
*****************************************************************************
*
* Jan Matousek, 2019-10-07
*/

#include <stdio.h>		// for printf
#include <math.h>		// for math functions and constants like pi
#include <fstream>		// for file reading with a stream
#include <string>		// for std::string
#include <sstream>		// for stream-based conversion from string to numbers

// include ROOT libraries that you need:
#include "TCanvas.h"
#include "TH1.h"		// 1-dimensional histograms

using namespace std;

int main(int argc, char **argv)
{
	// ******** ROOT histogramming exercise *********************************
	// With this example program, try to open the file events.txt 
	// or events_tail100.txt (containing the last 100 lines of events.txt)
	// They contain columns with numbers describing 
	// semi-inclusive deep inelastic events (SIDIS):
	// x    Q^2 (GeV)^2     q_h     z      P_hT (GeV)     phi_h (rad)
	// **********************************************************************
	printf("**** histo *****\n");
	if (argc < 2)
	{
		printf("Usage: histo INFILENAME\n");
		return 1;
	}
	const char* fname = argv[1];

	ifstream file(fname);	// take the 1st argument provided as the filename
	if (!file.is_open())
	{
		printf("Failed to open %s\n",fname);
		return 1;
	}
	printf("Opened file %s.\n",fname);
	//
	//	Create a histogram:
	//
	// For example a TH1D: a 1-dim. histogram using double to hold bin contents.
	//             TH1D(name,title,nbins,x0,x1)
	TH1D* hx = new TH1D("x","Hadrons in DIS",200,0.0,1.0);
	hx->GetXaxis()->SetTitle("#it{x}");	// axis label can be set like this

	TH1D* hz = new TH1D("z","Hadrons in DIS;#it{z}",200,0.0,1.0);
	// The axis label can be also set in a shorthand notation in title after ';'

	//
	// read the numbers from the file and fill the histogram:
	//
	double x, Qsq, z, pT, phi;
	int q;
	string line;
	while (getline(file, line))
	{
		if (line[0] == '#')	// if the first character is '#' -> just a comment
		{
			printf("Read comment: '%s'\n",line.c_str());
			continue;
		}
		istringstream stream(line);
		if (stream >> x >> Qsq >> q >> z >> pT >> phi)
		{
			hx->Fill(x);
			hz->Fill(z);
		}
		else
			printf("Failed to understand line '%s'\n",line.c_str());
	}
	TCanvas c("canv","",1200,600); // create a canvas named canv with 800x400 px
	c.Divide(2);
	c.cd(1);
	hx->Draw();
	c.cd(2);
	hz->Draw();
	c.Print("histo.pdf");
	return 0;
}
