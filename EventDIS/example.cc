/******************************************************************************
*	Example and testing of EventDIS class
*******************************************************************************
* Jan Matousek
*/

#include <stdio.h>              // for printf

#include "EventDIS.h"
#include "TFile.h"
#include "TTree.h"

using namespace std;

const double M_mu = 0.105658389;	// muon mass [GeV]
const int    MT   = 0x002;
const int    LT   = 0x004;
const int    OT   = 0x008;
const int    LAST = 0x200;

void testTreeCreation(const char* fName)
{
	Event* event = new Event();
	TFile* file = new TFile(fName, "recreate");

	TTree* tree = new TTree("DISEventsTree","DIS events");
	tree->Branch("event", &event);
//	event->SetBranchesForWriting(tree);

	event->Clear();
	event->SetEvent(275600,189,32601);
	event->SetTrigger(OT | LAST);

	event->AddVertex(true,true,0.23,0.52,-147.69);

	event->AddTrack(-1, 15, 20.0, 0.1);	// beam track
	event->LastTrack()->SetPoint(0.02,0.17,-463.8);
	event->LastTrack()->SetPoint(0.02,0.17,-463.8);
	event->LastTrack()->LinkVertex(0,true,-0.02,0.17,160.75);

	event->AddTrack(-1, 50, 70.0, 102.9);	// mu' track
	event->LastTrack()->SetPoint( -2.00,-0.45,   54.80);
	event->LastTrack()->SetPoint(-17.42,-21.74,4631.06);
	event->LastTrack()->LinkVertex(0,false,-1.65,-0.72,150.03);

	event->Print();
	tree->Fill();

	tree->Write();
	file->Close();
	printf("The Tree was saved into '%s'.\n\n",fName);
	delete event;
	delete file;
}

int testTreeReading(const char* fName,int maxEvent)
{
	TFile* file = new TFile(fName,"read");
	printf("Opened file '%s'.\n\n",fName);

	TTree* tree = NULL;
	file->GetObject("DISEventsTree",tree);
	if (tree == NULL)
		return 1;
	printf("The tree contains %lli entries.\n",tree->GetEntries());

	Event* event = new Event();
	if (tree->SetBranchAddress("event", &event) != 0)
	{
		printf("Failed to set the address of the 'event' branch.\n");
		return 2;
	}

	for (int i = 0; i < tree->GetEntries() && i < maxEvent; i++)
	{
		tree->GetEntry(i);
		event->Print();
		printf("\n");

		// An example of calculations with 4-vectors:
		PxPyPzMVector pBeam = event->BeamTrack()->FourMomAtVert(M_mu);
		printf("P_beam = (%.4f, %.4f, %.4f) GeV, E_beam = %.4f GeV\n",
			pBeam.Px(), pBeam.Py(), pBeam.Pz(), pBeam.E());
		PxPyPzMVector q = pBeam - event->MuPrTrack()->FourMomAtVert(M_mu);
		printf("Virtual photon 4-vector q = (%.4f, %.4f, %.4f, %.4f) GeV\n",
			q.Px(), q.Py(), q.Pz(), q.E());
		printf("Virtual photon mass |q| = %.4f GeV\n", q.mag());
		printf("Q^2 = -q^2 = %.4f GeV^2\n\n", -q.mag2());
	}

	file->Close();
	delete event;
	delete file;
	return 0;
}

int main(int argc, char** argv)
{
	const char* fname;
	int maxEvent = 10;

	printf(" *** Program to test saving and loading EventDIS structure ***\n");
	printf("Usage: example [ROOT_TREE_FILE] [MAX_EVENTS=%i]\n",maxEvent);

	if (argc == 1)
	{
		printf("\nNo tree file provided, creating a test tree...\n\n");
		fname = "test.root";
		testTreeCreation(fname);
	}
	else
		fname = argv[1];

	printf("\nReading the tree...\n\n");
	return testTreeReading(fname,maxEvent);
}
