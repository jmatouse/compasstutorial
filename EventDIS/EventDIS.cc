
#include "EventDIS.h"

/************************* R i c h P i d ************************************/
void RichPid::Print(const char* indent) const
{
	if (like[pi] < 0)
		printf("%sRICH likelihoods: not available\n",indent);
	else
		printf("%sRICH likelihoods: pi:%5.2f K:%5.2f p:%5.2f e:%5.2f mu:%5.2f bg:%5.2f\n",
			indent,like[pi],like[K],like[p],like[e],like[mu],like[bg]);
	if (std::isnan(mom.X()))
		printf("%sMomentum at RICH: not available\n",indent);
	else
		printf("%sMomentum at RICH: pX: %7.2f pY: %7.2f pZ: %7.2f (GeV/c)\n",
			indent,mom.X(),mom.Y(),mom.Z());
}


/*************************  T r a c k  **************************************/

void Track::LinkVertex(unsigned char iV, bool in, XYZVector pAtV)
	{ vertInds.push_back(iV); vertIsIn.push_back(in); vertPs.push_back(pAtV); }

void Track::LinkVertex(unsigned char iV,bool in,
		double pX,double pY,double pZ)
	{ XYZVector pAtV(pX,pY,pZ); LinkVertex(iV, in, pAtV); }

int Track::LocVertIndex(int iv) const
{
	for (unsigned int i = 0; i < vertInds.size(); i++)
	{
		if (vertInds[i] == iv)
			return i;
	}
	return -1;
}

PxPyPzMVector Track::FourMomAtVertLoc(double m,int i) const
{
	if (i>=0 && i<NVert())
	{
		PxPyPzMVector v(vertPs[i].X(),vertPs[i].Y(),vertPs[i].Z(),m);
		return v;
	}
	else
	{
		PxPyPzMVector v(NAN,NAN,NAN,NAN);
		return v;
	}
}

/*************************  E v e n t  ********************************/

void Event::Print() const
{
	printf("DIS Event (run %i, spill %i, event in spill %i) ---------------\n",
			run, spill, eInSpill);
	printf("  Triggers: 0x%04x\n", trigBits);
	printf("\n%3i Vertices: prim. inTarg. nOut%8s%8s%8s  in-  out-tracks\n",
			NVert(),"X","Y","Z (cm)");
	for (int i = 0; i < NVert(); i++)
	{
		printf("%12i: %5i %7i %4i  %6.2f  %6.2f  %6.1f", i,
			vertices[i].IsPrimary(),vertices[i].IsInTarget(),NTrackOutVertex(i),
			vertices[i].Position().X(),vertices[i].Position().Y(),
			vertices[i].Position().Z());
		int it = InTrackIndex(i);
		if (it >= 0)
			printf("%5i ",it);
		else
			printf("   -- ");
		for (int j = 0; j < NTrackOutVertex(i); j++)
			printf(" %i",OutTrackIndex(j,i));
		printf("\n");
	}

	printf("\n%3i Tracks:\n",NTracks());
	for (int i = 0; i < NTracks(); i++)
	{
		printf("\n%10i: q: %+2i, NDF: %3i chi2: %3.0f X/X0: %5.1f nVert: %2i\n",
			i, tracks[i].Q(),tracks[i].NDF(),tracks[i].ChiSq(),
			tracks[i].XX0(),tracks[i].NVert());
		printf("%11s%i associated vertices: iVert  in?%8s%8s%8s (GeV/c)\n","",
			tracks[i].NVert(),"pX","pY","pZ");
		for (int j = 0; j < tracks[i].NVert(); j++)
			printf("%39i  %3i  %6.2f  %6.2f  %6.2f\n",
				tracks[i].GlobVertIndex(j),   tracks[i].IsInTrackOfVertLoc(j),
				tracks[i].MomAtVertLoc(j).X(), tracks[i].MomAtVertLoc(j).Y(),
				tracks[i].MomAtVertLoc(j).Z());
		printf("%11s%i saved points:%3s%7s%8s (cm)\n",
			"",tracks[i].NPoints(),"X","Y","Z");
		for (int j = 0; j < tracks[i].NPoints(); j++)
			printf("%23s%6.2f %6.2f %7.2f\n","",
				tracks[i].MeasuredPoint(j).X(),
				tracks[i].MeasuredPoint(j).Y(),
				tracks[i].MeasuredPoint(j).Z());
		tracks[i].RichInfo()->Print("           ");
	}
	printf("End of DIS Event (run %i, spill %i, event in spill %i) -------\n\n",
			run, spill, eInSpill);
}

int Event::NTrackOutVertex(int iv) const
{
	if (iv < 0 || iv >= NVert())
		return -1;
	int n = -vertices[iv].IsPrimary();
	for (int it = 0; it < NTracks(); it++)
	{
		if (tracks[it].LocVertIndex(iv) >= 0)
			n++;
	}
	return n;
}

int Event::InTrackIndex(int iv) const
{
	if (iv < 0 || iv >= NVert() || !vertices[iv].IsPrimary())
		return -1;
	for (int it = 0; it < NTracks(); it++)
	{
		if (tracks[it].IsInTrackOfVert(iv))
			return it;
	}
	return -1;
}

int Event::OutTrackIndex(int it, int iv) const
{
	if (iv < 0 || iv >= NVert() || it < 0)
		return -1;
	for (int iT = 0; iT < NTracks(); iT++)
	{
		if (tracks[iT].IsOutTrackOfVert(iv))
		{
			if (it == 0)
				return iT;
			it--;
		}
	}
	return -1;
}
