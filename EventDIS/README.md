# DIS Event class

This class represents a deep-inelastic scattering event at COMPASS.
The Event objects can be saved in and read from a ROOT tree.

The `Event` object itself contains some basic info about the event
(run and spill number, a bit mask telling which triggers fired),
a vector of reconstructed vertices (`Vertex` objects) and a vector of
charged particle tracks associated to the vertices (`Track` objects).

In each event, there is the best primary vertex. That is the most probable
point where the beam muon scattered on a target proton. It has a beam track,
a scattered muon track and as many as possible other outgoing tracks associated.
There may be other primary vertices (vertices with a beam track). Some of them
are just other possible solutions of the vertex fit and share some tracks with
the best primary vertex.

Usually, there are also secondary vertices. Our reconstruction software looks
only for V-type vertices with no incoming and two outgoing tracks. They are
simply pairs of tracks that approach each other close enough.

The `Vertex` object stores the position of the vertex and whether it is primary.

The `Track` object stores the charge, the amount of material crossed by the track
(only muons can cross a lot of material) and the 3-momentum of the track at each
vertex associated to the track (it may differ a bit for different vertices).

There is no detailed documentation, but the header file (`EventDIS.h`) contains
comments and should be quite explanatory.



## Read the tree of Events in the interactive mode

Launch ROOT and compile and load the source code of the class:
```
root [0] .L EventDIS.cc+
```
Open the file containing the tree:
```
root [1] TFile file("tree.root","read")
```
We can check the contents:
```
root [2] .ls
TFile**		tree.root	PHAST histograms
 TFile*		tree.root	PHAST histograms
  KEY: TDirectoryFile	PhastHist;1	PhastHist
  KEY: TTree	DISEventsTree;1	SIDIS 2016/17 events
  KEY: TH1D	hCuts;1	Events after cuts
```
`hCuts` is a histogram showing how the number of events reduced during
the event selection procedure (when compacting the COMPASS trees).
`DISEventsTree` is the tree of `Events`. Let's check how many events are there:
```
root [3] DISEventsTree->GetEntries()
(long long) 4283
```
We can browse the file using the TBrowser tool, try typing
```
root [3] TBrowser tb
```
With the browser we can examine what data are in the tree and draw
the distributions of the saved quantities.
Try clicking on ROOT `files->tree.root->DISEventsTree->event->run`
A histogram of run numbers should show up (there is just 1 run in `tree.root`)
Try clicking, for example, on `BestPrimVert()->position->X()`.

We can draw the distributions also directly from the ROOT command prompt:
```
root [4] DISEventsTree->Draw("event.BeamTrack().MomAtVert().Z()")
```
2-dimensional distributions can be drawn with the usual syntax:
```
root [5] DISEventsTree->Draw("event.BestPrimVert().Position().Y() : event.BestPrimVert().Position().Z()")
```
Or we can connect an Event object to the tree:
```
root [6] Event* e = new Event()
(Event *) 0x55a87b242c10
root [7] DISEventsTree->SetBranchAddress("event", &e)
(int) 0
```
Load the first event (the properties of the object "e" will be set):
```
root [8] DISEventsTree->GetEntry(0)
(int) 2445
```
Now we can use any method of Event or its Vertices and Tracks:
```
root [9] e->BestPrimVert()->Position().Z()
(double) -75.112846
root [10] e->Print()
DIS Event (run 275800, spill 4, event in spill 13062) ---------------
  Triggers: 0x0008

  7 Vertices: prim. inTarg. nOut       X       Y  Z (cm)  in-  out-tracks
           0:     1       1    4    0.18    0.20   -75.1    0  1 2 3 4
           1:     0       0    2    8.74    4.31    73.0   --  5 6
           2:     0       0    2    7.05    3.60    38.0   --  5 7
           3:     0       1    2    0.50    0.39   -80.0   --  2 3
           4:     0       1    2    0.42    0.96   -76.7   --  3 4
           5:     0       0    2    0.66   -2.23   -98.2   --  3 8
           6:     0       0    2    3.30    4.63   -28.0   --  7 8

  9 Tracks:

         0: q: +1, NDF:  14 chi2:   7 X/X0:   0.1 nVert:  1
           1 associated vertices: iVert  in?      pX      pY      pZ (GeV/c)
                                      0    1    0.04   -0.08  157.09
           3 saved points:  X      Y       Z (cm)
                         0.07   0.40 -463.80
                         0.85  -0.28  600.00
                        -0.01   0.59 -759.80
           RICH likelihoods: not available
           Momentum at RICH: pX:    0.35 pY:   -0.10 pZ:  157.18 (GeV/c)

         1: q: +1, NDF:  84 chi2:  97 X/X0:  96.7 nVert:  1
           1 associated vertices: iVert  in?      pX      pY      pZ (GeV/c)
                                      0    0   -1.34   -1.63   98.88
           3 saved points:  X      Y       Z (cm)
                        -1.58  -1.93   54.80
                        -8.18 -10.96  600.00
                       -14.33 -75.74 4521.12
           RICH likelihoods: pi: 2.72 K: 2.70 p: 2.47 e: 2.71 mu: 2.72 bg: 0.02
           Momentum at RICH: pX:   -1.03 pY:   -1.63 pZ:   98.88 (GeV/c)
.
.
.
```
In this way, we can loop over all the events, fill histograms, calculate things...

## Read the tree of Events with a macro

Check the `example_macro.cc` file. It can be run in the interpreting mode,
```
root [0] .x macro_example.cc
```
or in the compiled mode,
```
root [0] .x macro_example.cc+
```
Without arguments, it tries to open `tree.root`. To open another file, do
```
root [0] .x macro_example.cc("another_file.root")
```
In any case, if you want to run it again, it is better to close ROOT (`.q`)
and start a new ROOT session. It is handy to run the macro
directly from the command line:
```
jan@somepc: ~/.../EventDIS/ $ root macro_example.cc
```
One can also run it compiled,
```
jan@somepc: ~/.../EventDIS/ $ root macro_example.cc+
```
To open another tree file, we must add quotes:
```
jan@somepc: ~/.../EventDIS/ $ root 'macro_example.cc("file.root")'
```

## Read the tree of Events using a compiled program

Check the `example.cc` source code.

The program must be compiled. This job is done by the `Makefile`, where
the program is also linked with ROOT and with the `EventDIS` class.
You can run it typing `make` in your command line.
