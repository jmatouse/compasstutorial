/******************************************************************************
*	Example macro showing how to read a tree of DIS Events
*******************************************************************************
* Jan Matousek
*/

// Include ROOT classes and other header files
// (this is mandatory only if you want to compile the macro)
#include "TFile.h"
#include "TH1.h"

// Include the EventDIS source (the header file is not enough in this case,
// as ROOT would not know where to find the bodies of the functions)
#include "EventDIS.cc"


void macro_example(const char* fileName = "hist.root")
{
	printf(" DIS Event tree reading macro example\n");
	TFile* file = new TFile(fileName,"read");
	if (!file->IsOpen())
	{
		printf("Failed to open file '%s'.\n",fileName);
		return;
	}
	printf("Opened file '%s'.\n",fileName);

	TTree* tree = NULL;
	file->GetObject("DISEventsTree", tree);
	if (tree == NULL)
	{
		printf("Failed to load 'DISEventsTree' from the file.\n");
		return;
	}

	Event* e = new Event();
	tree->SetBranchAddress("event", &e);

	// For example, let's look at the distributions of beam momenta at the best
	// primary vertices:
	TH1D* hE = new TH1D("hE", ";#it{E}_{#mu} (GeV);entries", 100, 120.0, 200.0);
	// and at the position of all the vertices:
	TH1D* hZv = new TH1D("hZv", ";#it{Z}_{v} (cm);entries", 100, -400.0, 0.0);

	for (int i = 0; i < tree->GetEntries(); i++)	// loop over all the events
	{
		tree->GetEvent(i);	// the properties of the i-th event are copied to e.
//		e.Print(); // print the event content

		// Fill the momentum of the beam track at the best primary vertex
		// (e->BeamTrack() is a pointer to the beam Track of the best PV,
		// MomAtVert() is the momentum XYZVector of the Track at the best PV)
		hE->Fill(sqrt(e->BeamTrack()->MomAtVert().mag2()));

		for (int j = 0; j < e->NVert(); j++)
		{
			Vertex* v = e->Vert(j);
			hZv->Fill(v->Position().Z());
		}
	}

	TCanvas* c = new TCanvas("c","",1200,600);
	c->Divide(2);
	c->cd(1);
	hE->Draw();
	c->cd(2);
	hZv->Draw();
}
