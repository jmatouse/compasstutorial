#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class Event+;
#pragma link C++ class Track+;
#pragma link C++ class Vertex+;
#pragma link C++ class RichPid+;
#endif
