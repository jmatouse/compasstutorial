/******************************************************************************
*	This class represents a DIS event at COMPASS
*******************************************************************************
* Jan Matousek
*/

#ifndef __EVENT_DIS__
#define __EVENT_DIS__

#include "TObject.h"
#include "Math/Point3D.h"
#include "Math/Vector3D.h"
#include "Math/Vector4D.h"
#include "TTree.h"

using namespace ROOT::Math;
using namespace std;

class RichPid
{
	public:
	static const int N  = 6;	// number of PID values
	static const int pi = 0;	// pion
	static const int K  = 1;	// kaon
	static const int p  = 2;	// proton
	static const int e  = 3;	// electron
	static const int mu = 4;	// muon
	static const int bg = 5;	// background	
	private:
	float like[N];
	XYZVector mom;

	public:
	RichPid()
		{ for(int i=0; i<N; i++) like[i] = -1.; mom = XYZVector(NAN,NAN,NAN); }
	void Set(float like[N], XYZVector momAtRich)
		{ for(int i=0; i<N; i++) this->like[i] = like[i]; mom = momAtRich; }
	float Like(int pid) const
		{ if(pid>=0 && pid<N) return like[pid]; else return NAN;}
	const XYZVector* Mom() const { return &mom; }
	void Print(const char* indent="") const;
};

class Track
{
	signed char  q, iRich;	// charge, index of measured point at RICH entrance
	unsigned char ndf;
	float chiSq, xx0;
	RichPid richPid;
	vector<unsigned char> vertInds;	// indices of vertices assoc. to track
	vector<bool> vertIsIn;		// is this track is incoming (or outgoing)?
	vector<XYZVector> vertPs;   // momenta at the vertices
	vector<XYZPoint>  points;   // first measured point, last measured point

	public:

	Track();
	Track(signed char q, int ndf, float chiSq, float xx0);
	void LinkVertex(unsigned char iV,bool in,XYZVector pAtV);
	void LinkVertex(unsigned char iV,bool in,double pX,double pY,double pZ);
	void SetPoint(double X,double Y,double Z);
	void SetRichInfo(int iP, float richLike[RichPid::N],double pX,double pY,double pZ);
	const RichPid* RichInfo() const { return &richPid; }

	/************************************************************************
	*     Methods for reading the Track object without changing it:         *
	************************************************************************/

	int Q()   const { return q; }   /// track charge = -1 or +1
	int NDF() const { return ndf; } /// num. of degrees of freedom of track fit
	double ChiSq() const { return chiSq; } /// chi^2 of the track fit
	double XX0()   const { return xx0; }   /// X/X0
	/**
	*	Returns the number of radiation lengths crossed by the track.
	*/

	int NVert() const;  /// Number of vertices associated to the track.
	int GlobVertIndex(int i) const; /// The global index of i-th vert. assoc.
	int LocVertIndex(int iV=0) const; /// The local index of the vertex iV.
	/**
	* iV is the 'global' vertex index (the index in Event::Vertices).
	* The local vertex index numbers the vertices assiciated to this track.
	* If the vertex iV is not associated to this track, LocVertIndex returns -1.
	*/
	bool IsInTrackOfVertLoc(int i) const; /// Is the track incoming to the vert?
	XYZVector MomAtVertLoc(int i) const; /// 3-momentum at i-th assoc. vertex.
	PxPyPzMVector FourMomAtVertLoc(double m,int i) const; /// 4-momentum...

	bool IsInTrackOfVert(int iV=0) const; /// Is this incom. track of vert. iV?
	bool IsOutTrackOfVert(int iV=0) const; /// Is this out. track of vert. iV?
	XYZVector MomAtVert(int iV=0) const; /// Track momentum at vertex iV.
	PxPyPzMVector FourMomAtVert(double m,int iV=0) const; /// 4-mom. at vert. iV

	int NPoints() const;	/// number of saved points
	/**
	* Position and momentum of the track at specified points is saved.
	* The first measured point and the last one are always there.
	*/
	XYZPoint MeasuredPoint(int iP) const;
	XYZPoint FirstMeasuredPoint() const { return MeasuredPoint(0); }
	double ZFirst() const;	/// the Z of the first measured point
	XYZPoint LastMeasuredPoint() const  { return MeasuredPoint(NPoints()-1); }
	double ZLast() const;	/// the Z of the last measured point
	XYZPoint  PosAtRICH() const         { return MeasuredPoint(iRich); }
};

class Vertex
{
	bool     isPrimary, inTarget;
	XYZPoint position;

	public:

	Vertex();
	Vertex(bool isPrim, bool inTarg, XYZPoint pos);

	/************************************************************************
	*     Methods for reading the Vertex object without changing it:        *
	************************************************************************/
	bool IsPrimary() const { return isPrimary; } /// Primary = with incoming tr.
	bool IsInTarget() const { return inTarget; } /// Is it in the target?
	XYZPoint Position() const { return position; } /// Vertex position.
};

class Event
{
	int run, spill, eInSpill;
	unsigned short trigBits;
	vector<Track>  tracks;
	vector<Vertex> vertices;

	public:

	Event();
	void Clear();
	void Print() const;

	void SetEvent(int run, int spill, int eInSpill);
	void SetTrigger(int trigBits);
	void AddTrack(signed char q, int ndf, float chiSq, float xx0);
	Track* Tracks(int i);
	Track* LastTrack();
	Track* BeamTrack();
	Track* MuPrTrack();

	void AddVertex(bool isPrim, bool inTarg, double x, double y, double z);
	Vertex* Vert(int iv);	/// the iv-th vertex
	Vertex* LastVertex();

	/************************************************************************
	*     Methods for reading the Event object without changing it:        *
	************************************************************************/
	int Run()   const { return run; }   /// The run number.
	int Spill() const { return spill; } /// The spill number (within the run).
	int EvInSpill() const { return eInSpill; }	/// The event-in-spill number.
	int NTracks()  const;	/// number of saved tracks in the event
	int NVert()  const;	/// number of prim. and sec. vertices saved
	const Vertex* BestPrimVert() const;	/// the best primary vertex (the 1st)
	const Track*  InTrack(int iv=0) const; /// track incoming to vertex iv
	/**
	*	Returns a pointer to the track incoming to vertex iv.
	*	The default argument iv=0 means that the best primary vertex is used.
	*	If the vertex is secondary (no incoming track), NULL is returned.
	*/
	int InTrackIndex(int iv=0) const;	/// the index of the incoming track
	int NTrackOutVertex(int iv=0) const; /// number of tracks going out of iv
	const Track* OutTrack(int i, int iv=0) const;	/// outgoing tracks
	/**
	*	Returns a pointer to the i-th track outgoing from the vertex iv.
	*	(iv is the vertex index, 0 <= i < NTrackOutVertex(iv))
	*/
	int OutTrackIndex(int it, int iv=0) const;	/// indices of outgoing tracks
};


//*** inline function bodies for Track ****
inline Track::Track()
	{ q = 0; xx0 = NAN; chiSq = NAN; ndf = 0; iRich = -1; }
inline Track::Track(signed char q, int ndf, float chiSq, float xx0)
{
	this->q = q; this->xx0 = xx0; this->chiSq = chiSq;
	if (ndf >= 0 && ndf <= 255) this->ndf = ndf; else this->ndf = 255;
}
inline void Track::SetPoint(double X,double Y,double Z)
	{ XYZPoint  x(X,Y,Z);    points.push_back(x); }
inline void Track::SetRichInfo(int iP,float richLike[RichPid::N],double pX,double pY,double pZ)
	{ iRich = iP; richPid.Set(richLike,XYZVector(pX,pY,pZ)); }
inline int Track::NVert() const
	{ return vertInds.size(); }
inline int Track::GlobVertIndex(int i) const
	{ if (i>=0 && i<NVert()) return vertInds[i]; else return -1; }

inline bool Track::IsInTrackOfVertLoc(int i) const
	{ if (i>=0 && i<NVert()) return vertIsIn[i]; else return false; }
inline XYZVector Track::MomAtVertLoc(int i) const
{
	if (i>=0 && i<NVert()) return vertPs[i];
	else { XYZVector v(NAN,NAN,NAN); return v; }
}

inline bool Track::IsInTrackOfVert(int iv) const
	{ int i=LocVertIndex(iv); if(i>=0) return vertIsIn[i]; else return false; }
inline bool Track::IsOutTrackOfVert(int iv) const
	{ int i=LocVertIndex(iv); if(i>=0) return !vertIsIn[i]; else return false; }
inline XYZVector Track::MomAtVert(int iv) const
	{ int i=LocVertIndex(iv); return MomAtVertLoc(i); }
inline PxPyPzMVector Track::FourMomAtVert(double m,int iv) const
	{ int i=LocVertIndex(iv); return FourMomAtVertLoc(m,i); }

inline int Track::NPoints() const
	{ return points.size(); }
inline XYZPoint Track::MeasuredPoint(int iP) const
{
	if(iP >= 0 && iP < NPoints()) return points[iP];
	else { XYZPoint p(NAN,NAN,NAN); return p; }
}
inline double Track::ZFirst() const
	{ if(NPoints() > 0) return points[0].Z(); else return NAN; }
inline double Track::ZLast() const
	{ if(NPoints() > 1) return points.back().Z(); else return NAN; }

//*** inline function bodies for Vertex ****

inline Vertex::Vertex()
	{ isPrimary = false; inTarget = false; }
inline Vertex::Vertex(bool isPrim, bool inTarg, XYZPoint pos)
	{ isPrimary = isPrim; inTarget = inTarg; position = pos; }

//*** inline function bodies for Event ****

inline Event::Event()
	{ Clear(); }
inline void Event::Clear()
{
	run = -1; spill = -1; eInSpill = -1; trigBits = 0;
	tracks.clear(); vertices.clear();
}
inline void Event::SetEvent(int run, int spill, int eInSpill)
	{ this->run = run; this->spill = spill; this->eInSpill = eInSpill; }
inline void Event::SetTrigger(int trigBits)
	{ this->trigBits = trigBits & 0xFFF;} // The non-relevant part is truncated.
inline int Event::NTracks()  const
	{ return tracks.size(); }
inline int Event::NVert()  const
	{ return vertices.size(); }
inline Track* Event::Tracks(int i)
	{ if(i>=0 && i<NTracks()) return &(tracks[i]); else return NULL; }
inline Track* Event::LastTrack()
	{ if(NTracks()>0) return &(tracks.back()); else return NULL; }
inline Track* Event::BeamTrack()
	{ if(NTracks() > 0) return &(tracks[0]); else return NULL; }
inline Track* Event::MuPrTrack()
	{ if(NTracks() > 1) return &(tracks[1]); else return NULL; }
inline void Event::AddTrack(signed char q, int ndf, float chiSq, float xx0)
	{ Track tr(q,ndf,chiSq,xx0); tracks.push_back(tr); }
inline void Event::AddVertex(bool isPrim,bool inTarg,double x,double y,double z)
{
	assert(NVert() < 255); // otherwise char Track::vertInds will not work
	XYZPoint X(x,y,z); Vertex v(isPrim,inTarg,X); vertices.push_back(v);
}
inline Vertex* Event::Vert(int iv)
	{ if(iv>=0 && iv<NVert()) return &(vertices[iv]); else return NULL; }
inline Vertex* Event::LastVertex()
	{ if(NVert()>0) return &(vertices.back()); else return NULL; }
inline const Vertex* Event::BestPrimVert() const
	{ if(NVert() > 0) return &(vertices[0]); return NULL; }
inline const Track*  Event::InTrack(int iv) const
	{ int i=InTrackIndex(iv); if(i>=0) return &(tracks[i]); else return NULL;}
inline const Track*  Event::OutTrack(int i, int iv) const
	{ int j=OutTrackIndex(i,iv); if(j>=0) return &(tracks[j]); else return NULL;}

#endif
