# COMPASS tutorial

Introduction into COMPASS data analysis. Examples are given on how one can use [ROOT](https://root.cern.ch/) to analyse physics data in the context of COMPASS. It is not a ROOT manual, we recommend the readers to refer to it for explanations and deeper understanding. The examples also assume the help of an experienced colleague.

- [CppInOut](./CppInOut): write and compile standalone C++ application, read a file, read the standard input, read arguments given to the program.

- [UseRoot](./UseRoot): three ways of using ROOT: interactive, macro and ROOT-linked standalone C++ application.

- [Histograms](./Histograms): read a text file with deep inelastic scattering (DIS) events and create 1-dimensional and 2-dimensional histrograms of various quantities. Set logarithmic binning to a histogram axis.

- [Trees](./Trees): open a ROOT file and read a ROOT tree with branches containing momentum vectors of the incoming and scattered muon and of the outgoing hadrons of DIS events. Operations with vectors. DIS kinematic variables.

- [Histograms 2](./Histograms2/): study a multi-dimensional distribution &ndash; create several histograms of one variable and select in which one to fill the event based on several other variables. Do it using an array of histograms.

- [EventDIS](./EventDIS/): read a tree containing custom class objects, which represent a deep-inelastic event and all the produced charged tracks with all the reconstructed vertices.
