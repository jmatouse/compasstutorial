# Using ROOT libraries in C++ programs, interactively and in macros

## Linking ROOT libraries

Look at the example of a program that uses ROOT libraries `rootlinked.cpp`.
Note that one needs to `include` the libraries first (each ROOT class, for
example `TGraph` or `TCanvas`, is a separate library).
Then look in the Makefile to see how the `rootlinked.cpp` code is compiled
into an object file (`rootlinked.o`) and finally
linked with the libraries into an executable (`rootlinked.exe`).

- Write your own program that will, in addition to drawing the `TGraph`, also fit
it with a function (look at [TF1 class](https://root.cern.ch/doc/master/classTF1.html) and [TGraph::Fit()](https://root.cern.ch/doc/master/classTGraph.html#a61269bcd47a57296f0f1d57ceff8feeb)).

- Replace `TGraph` with a `TGraphErrors` (it needs 2 more arrays of doubles -
the errors in x and y. For those in x create an array containing only zeros,
```cpp
double* ex = new double[n];
for (int i = 0; i < n; i++)
	ex[i] = 0;
```

- Try to modify the program so it opens 3 files instead of 1 and draws all
to the same canvas (for the 2nd and 3rd graph, use drawing option `"same"`,
you will find several data files in `../CppInOut/`).

- Check the web references for `TGraphPainter`, `TAttMarker` and `TAttLine` and
try changing the appearance of the graphs and functions (colors, marker styles,
line styles)

- Finally, create a fourth `TGraphErrors` with 3 points, where the x-axis variable
will be just 1,2,3, the y-axis variable will be the cos(phi) modulation
amplitude from the three fits (he parameter 1 or 2, use `f.GetParameter(1)`)
and the y-error the error of the parameter.

## Interactive usage and ROOT macros

Try to do the same 'interactively'. ROOT is not juts a collection of libraries,
it is also an interpreter of C++ code. Try typing `root` in your terminal.
You will get a prompt, where you can type code in C++ and it will be immediately
executed. Look at this [presentation](https://docs.google.com/presentation/d/189f0qsDEnMSk2R5KWLRPz2TdEV5kTfXH1VcuAra4cnU/edit#slide=id.g2880f7ccdd_0_151)
to get some idea how it works.

Try some examples in the chapter "The ROOT Prompt and Macros - ROOT as a calculator, Controlling ROOT".

Then try to load our data using `TGraph` constructor that reads data directly from
the file:
```cpp
root[0] TGraph g("../CppInOut/data0.txt","%lg %lg")
root[1] g.Draw()
```

Try also TGraphErrors:
```cpp
root[0] TGraphErrors ge("../01_in-out/data0.txt","%lg %lg %lg")
root[1] ge.Draw()
```

Try changing the colors, styles, axs lables
(from the prompt and also by clicking at things in the window).

Declare the fit function and fit like we did in the compiled code.

Finally, try to create a macro -- a simple C++ file without any includes and
without function 'main'. In this file, write a function named for example
'PlotGraph' and containing some commands that you have used interactively before
for example:
```cpp
void PlotGraph()
{
	TGraphErrors ge("../01_in-out/data0.txt","%lg %lg %lg");
	TF1 f("f","[0] + [1]*cos(x) + [2]*sin(x)");
	ge.Fit(&f);
}
```
Then go to ROOT prompt and load the macro (`.L FILENAME`). Then you should be
able to execute the function typing just `PlotGraph()`.
