/****************************************************************************
*
*	use root libraries in a simple program
*
*****************************************************************************
*
* Jan Matousek, 2019-10-07
*/

#include <stdio.h>		// for printf etc.
#include <math.h>		// for math functions and constants like pi
#include <fstream>

// include ROOT libraries that you need:
#include "TCanvas.h"
#include "TGraph.h"		// graphs with errorbars
#include "TF1.h"

using namespace std;

int main(int argc, char **argv)
{
	printf("**** cpp_in-out 2 *****\n");
	if (argc < 2)
	{
		printf("Usage: cpp_in-out INFILENAME\n");
		return 1;
	}
	const char* fname = argv[1];

	ifstream file(fname);	// take the 1st argument provided as the filename
	if (!file.is_open())
	{
		printf("Failed to open %s\n",fname);
		return 1;
	}

	//
	// count the number of lines in the file:
	//
	double tmp;
	int n = 0;
	while (file >> tmp >> tmp >> tmp)	// read 3 numbers as long as you can
		n++;
	printf("Opened file %s with %i lines.\n",fname,n);
	// the files data0.txt, data0.txt and data0.txt contain 3 columns of numbers
	double* x = new double[n];
	double* y = new double[n];
	double* e = new double[n];	// uncertainty (1 sigma)

	file.clear(); // clear the error state that occured when it reached the end
	file.seekg(0,ios::beg);	// go to the beginning of the file
	//
	// read the numbers from the file:
	//
	int i = 0;
	while (i < n && file >> x[i] >> y[i] >> e[i])
		i++;

	TGraph graph(n,x,y); // create object TGraph with n points given in 2 arrays

	TCanvas c("canv","",800,600); // create a canvas named canv with 800x400 px
	// try playing with the options you can find at
	// https://root.cern.ch/doc/master/classTGraphPainter.html
	// e.g graph.SetMarkerStyle(6); graph.Draw("AP") instead of just Draw();
	graph.Draw();

	// create a function:
	TF1 f("f","[0]*(1 + [1]*cos(x) + [2]*sin(x))");
	graph.Fit(&f);

	c.Print("canv.pdf");

	return 0;
}
